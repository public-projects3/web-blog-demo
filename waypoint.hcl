project = "webblog"

app "webblog" {
  labels = {
    "service" = "webblog",
    "env"     = "dev"
  }

  url {
    auto_hostname = false
  }

  build {
    use "docker" {}
    registry {
      use "docker" {
        image = "registry.gitlab.com/public-projects3/web-blog-demo"
        tag   = gitrefpretty()
        // Uncomment the line below to run waypoint locally from your computer if you don't want to manually log into GitLab. Leave it commented to use it with GitLab's CI/CD
        // encoded_auth = filebase64("dockerAuth.json")
      }
    }
  }

  deploy {
    use "kubernetes" {
      probe_path  = "/login"
      context     = "gke_sam-gabrail-gcp-demos_us-central1-a_vault-cluster-demo"
      service_port = 80
      namespace = "webblog"
      service_account = "webblog"
      annotations = {
        "consul.hashicorp.com/connect-inject": "true"
        "consul.hashicorp.com/connect-service-upstreams": "webblog-mongodb:27017:dc1"
        "vault.hashicorp.com/agent-inject": "true"
        "vault.hashicorp.com/agent-inject-status": "update"
        "vault.hashicorp.com/agent-inject-token": "true"
        "vault.hashicorp.com/agent-limits-cpu": "250m"
        "vault.hashicorp.com/agent-limits-mem": "64Mi"
        "vault.hashicorp.com/agent-requests-cpu": "128m"
        "vault.hashicorp.com/agent-requests-mem": "32Mi" 
        "vault.hashicorp.com/role": "webblog"
        "vault.hashicorp.com/secret-volume-path": "/app/secrets/"
        "vault.hashicorp.com/agent-inject-secret-.envapp": "internal/data/webblog/mongodb"
        "vault.hashicorp.com/agent-inject-template-.envapp": "|{{- with secret \"internal/data/webblog/mongodb\" -}} DB_USER={{ .Data.data.username }} DB_PASSWORD={{ .Data.data.password }} {{- end -}}"
      }
    }
  }

  release {
    use "kubernetes" {
      load_balancer = true
      port          = 80
      namespace = "webblog"
    }
  }
}
