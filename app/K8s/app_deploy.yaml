apiVersion: v1
kind: ServiceAccount
metadata:
  name: webblog
  namespace: webblog
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: webblog
  name: webblog
  namespace: webblog
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: http
  - name: https
    port: 443
    protocol: TCP
    targetPort: https
  - name: metrics
    port: 9117
    protocol: TCP
    targetPort: metrics
  selector:
    app: webblog
  sessionAffinity: None
  type: NodePort
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress-webblog
  namespace: webblog
  annotations:
    kubernetes.io/ingress.global-static-ip-name: webblog-ip
  labels:
    app: webblog
spec:
  rules:
  - http:
      paths:
      - path: /*
        backend:
          serviceName: webblog
          servicePort: 80
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: webblog
  name: webblog-frontend
  namespace: webblog
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      app: webblog  
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        prometheus.io/port: "9117"
        prometheus.io/scrape: "true"
        consul.hashicorp.com/connect-inject: "true"
        consul.hashicorp.com/connect-service-upstreams: "webblog-mongodb:27017:dc1"
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/agent-inject-status: "update"
        vault.hashicorp.com/agent-inject-token: "true"
        vault.hashicorp.com/agent-limits-cpu: "250m"
        vault.hashicorp.com/agent-limits-mem: "64Mi"
        vault.hashicorp.com/agent-requests-cpu: "128m"
        vault.hashicorp.com/agent-requests-mem: "32Mi" 
        vault.hashicorp.com/role: "webblog"
        vault.hashicorp.com/secret-volume-path: "/app/secrets/"
        vault.hashicorp.com/agent-inject-secret-.envapp: "internal/data/webblog/mongodb"
        vault.hashicorp.com/agent-inject-template-.envapp: |
          {{- with secret "internal/data/webblog/mongodb" -}}
          DB_USER={{ .Data.data.username }}
          DB_PASSWORD={{ .Data.data.password }}
          {{- end -}}
      creationTimestamp: null
      labels:
        app: webblog
    spec:
      serviceAccountName: webblog
      imagePullSecrets:
      - name: regcred
      containers:
      - name: frontend      
        image: registry.gitlab.com/public-projects3/web-blog-demo:<VERSION>
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 6
          httpGet:
            httpHeaders:
            - name: X-Forwarded-Proto
              value: https
            path: /login
            port: http
            scheme: HTTP
          initialDelaySeconds: 120
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 5
        ports:
        - containerPort: 80
          name: http
          protocol: TCP
        - containerPort: 443
          name: https
          protocol: TCP
        readinessProbe:
          failureThreshold: 6
          httpGet:
            httpHeaders:
            - name: X-Forwarded-Proto
              value: https
            path: /login
            port: http
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 5
        # resources:
        #   requests:
        #     cpu: 150m
        #     memory: 128Mi
